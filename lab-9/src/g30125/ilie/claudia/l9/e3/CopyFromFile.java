package g30125.ilie.claudia.l9.e3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopyFromFile extends JFrame {

    JButton button;
    JLabel file;
    JTextField textField, tFile;

    CopyFromFile(){
        setTitle("File Display");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(280,410);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        file = new JLabel("File Name:");
        file.setBounds(10,50,100,30);
        file.setBackground(Color.white);

        tFile = new JTextField();
        tFile.setBounds(80,50,170,30);

        button = new JButton("Search");
        button.setBounds(10,100,100,30);
        button.setBackground(Color.cyan);

        textField = new JTextField();
        textField.setBounds(10,150,150,200);

        button.addActionListener(new checkIfPressed());

        add(file);
        add(tFile);
        add(button);
        add(textField);

    }


    class checkIfPressed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            BufferedReader br;
            String fileName = CopyFromFile.this.tFile.getText();
            String fileContent="";
            try {
                Path path = Paths.get(fileName);
                if (Files.exists(path)){
                    br = new BufferedReader(new FileReader(fileName));
                    String line = br.readLine();
                    while (line != null){
                        fileContent=fileContent+"\n"+line;
                        line=br.readLine();
                    }
                    CopyFromFile.this.textField.setText(fileContent);
                } else{
                    CopyFromFile.this.textField.setText("File cannot be found.");
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new CopyFromFile();
    }

}
