package g30125.ilie.claudia.l9.e2;

import java.awt.*;
import java.awt.event.*;

public class Counter extends Frame implements ActionListener {
    private Label lblCount;     // Declare component Label
    private TextField tfCount;  // Declare component TextField
    private Button btnCount;    // Declare component Button
    private int count = 0;      // counter's value


    public Counter () {
        setLayout(new FlowLayout());
        setSize(250, 100);
        setTitle("Counter");
        setVisible(true);

        lblCount = new Label("Counter:");


        tfCount = new TextField(String.valueOf(count), 4);
        tfCount.setEditable(false);


        btnCount = new Button("Count");
        btnCount.addActionListener(this);

        add(lblCount);
        add(tfCount);
        add(btnCount);

    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        count++;
        tfCount.setText(String.valueOf(count));
    }

    public static void main(String[] args) {
        new Counter();
    }
}