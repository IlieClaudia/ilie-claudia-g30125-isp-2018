package g30125.ilie.claudia.l9.e4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Xand0 extends JFrame {
    ArrayList<JButton> btn = new ArrayList<JButton>(11);
    JTextArea txta;
    static int cnt=0;
    static String lV = " ";


    Xand0(){
        setTitle("TicTacTe");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(4, 3));
        for (int i = 0; i < 9; i++) {
            JButton b = new JButton("click"+String.valueOf(i));
            btn.add(b);
            add(b);
        }
        //playing buttons
        btn.get(0).addActionListener(new PreB0());
        btn.get(1).addActionListener(new PreB1());
        btn.get(2).addActionListener(new PreB2());
        btn.get(3).addActionListener(new PreB3());
        btn.get(4).addActionListener(new PreB4());
        btn.get(5).addActionListener(new PreB5());
        btn.get(6).addActionListener(new PreB6());
        btn.get(7).addActionListener(new PreB7());
        btn.get(8).addActionListener(new PreB8());
        txta=new JTextArea("Tie");
        add(txta);
        JButton b=new JButton("X starts. \nClick for O");
        btn.add(b);
        add(btn.get(9));
        btn.get(9).addActionListener(new PreB9());
        JButton br=new JButton("Restart");
        btn.add(br);
        add(btn.get(10));
        btn.get(10).addActionListener(new PreB10());
        setSize(600, 600);
        setVisible(true);

    }

    public class PreB0 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(0).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(0).setText("O");
                    lV = "O";
                } else {
                    btn.get(0).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(0).getText().equals(btn.get(1).getText())) && (btn.get(0).getText().equals(btn.get(2).getText())))//l
                        || ((btn.get(0).getText().equals(btn.get(3).getText())) && (btn.get(0).getText().equals(btn.get(6).getText())))//c
                        || ((btn.get(0).getText().equals(btn.get(4).getText())) && (btn.get(0).getText().equals(btn.get(8).getText()))))//pd
                    txta.setText("winner: " + btn.get(0).getText());
            }
        }
    }

    public class PreB1 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(1).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(1).setText("O");
                    lV = "O";
                } else {
                    btn.get(1).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(0).getText().equals(btn.get(1).getText())) && (btn.get(0).getText().equals(btn.get(2).getText())))
                        || ((btn.get(1).getText().equals(btn.get(4).getText())) && (btn.get(1).getText().equals(btn.get(7).getText()))))
                    txta.setText("winner: " + btn.get(1).getText());
            }
        }
    }

    public class PreB2 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(2).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(2).setText("O");
                    lV = "O";
                } else {
                    btn.get(2).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(0).getText().equals(btn.get(1).getText())) && (btn.get(0).getText().equals(btn.get(2).getText()))) //linie
                        || ((btn.get(2).getText().equals(btn.get(5).getText())) && (btn.get(2).getText().equals(btn.get(8).getText()))) //coloana
                        || ((btn.get(2).getText().equals(btn.get(4).getText())) && (btn.get(2).getText().equals(btn.get(4).getText()))) ) //diagonala
                    txta.setText("winner: " + btn.get(2).getText());
            }
        }
    }


    public class PreB3 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(3).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(3).setText("O");
                    lV = "O";
                } else {
                    btn.get(3).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(3).getText().equals(btn.get(4).getText())) && (btn.get(3).getText().equals(btn.get(5).getText())))
                        || ((btn.get(3).getText().equals(btn.get(0).getText())) && (btn.get(3).getText().equals(btn.get(6).getText()))))
                    txta.setText("winner: " + btn.get(3).getText());
            }
        }
    }



    public class PreB4 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(4).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(4).setText("O");
                    lV = "O";
                } else {
                    btn.get(4).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(3).getText().equals(btn.get(4).getText())) && (btn.get(4).getText().equals(btn.get(5).getText()))) //linie
                        || ((btn.get(1).getText().equals(btn.get(4).getText())) && (btn.get(4).getText().equals(btn.get(7).getText()))) //coloana
                        || ((btn.get(2).getText().equals(btn.get(4).getText())) && (btn.get(2).getText().equals(btn.get(4).getText())))
                        || ((btn.get(0).getText().equals(btn.get(4).getText())) && (btn.get(8).getText().equals(btn.get(4).getText()))) ) //diagonala
                    txta.setText("winner: " + btn.get(4).getText());
            }
        }
    }


    public class PreB5 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(5).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(5).setText("O");
                    lV = "O";
                } else {
                    btn.get(5).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(3).getText().equals(btn.get(4).getText())) && (btn.get(3).getText().equals(btn.get(5).getText())))
                        || ((btn.get(5).getText().equals(btn.get(2).getText())) && (btn.get(5).getText().equals(btn.get(8).getText()))))
                    txta.setText("winner: " + btn.get(5).getText());
            }
        }
    }


    public class PreB6 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(6).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(6).setText("O");
                    lV = "O";
                } else {
                    btn.get(6).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(6).getText().equals(btn.get(7).getText())) && (btn.get(6).getText().equals(btn.get(8).getText())))
                        || ((btn.get(6).getText().equals(btn.get(3).getText())) && (btn.get(6).getText().equals(btn.get(0).getText())))
                        || ((btn.get(6).getText().equals(btn.get(4).getText())) && (btn.get(6).getText().equals(btn.get(2).getText()))))
                    txta.setText("winner: " + btn.get(6).getText());
            }
        }
    }


    public class PreB7 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(7).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(7).setText("O");
                    lV = "O";
                } else {
                    btn.get(7).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(6).getText().equals(btn.get(7).getText())) && (btn.get(7).getText().equals(btn.get(8).getText())))
                        || ((btn.get(7).getText().equals(btn.get(4).getText())) && (btn.get(7).getText().equals(btn.get(1).getText()))))
                    txta.setText("winner: " + btn.get(7).getText());
            }
        }
    }

    public class PreB8 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!(btn.get(8).getText().equals(lV))) {
                cnt++;
                if (cnt % 2 == 0) {
                    btn.get(8).setText("O");
                    lV = "O";
                } else {
                    btn.get(8).setText("X");
                    lV = "X";
                }//btn.get().setText(String.valueOf();
                if (       ((btn.get(6).getText().equals(btn.get(7).getText())) && (btn.get(6).getText().equals(btn.get(8).getText())))
                        || ((btn.get(8).getText().equals(btn.get(5).getText())) && (btn.get(8).getText().equals(btn.get(2).getText())))
                        || ((btn.get(0).getText().equals(btn.get(4).getText())) && (btn.get(0).getText().equals(btn.get(8).getText()))))
                    txta.setText("winner: " + btn.get(8).getText());
            }
        }
    }

    public class PreB9 implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            cnt++;
            if (cnt%2==1) btn.get(9).setText("O starts. \nClick for X");
            else btn.get(9).setText("X starts. \nClick for O");

        }
    }

    public class PreB10 implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            cnt=0;
            txta.setText("Tie");
            btn.get(9).setText("X starts \nClick for O");
            for (int i = 0; i < 9; i++) {
                btn.get(i).setText("click"+String.valueOf(i));
            }
        }
    }

    public static void main(String[] args) {
        new Xand0();
    }
}
