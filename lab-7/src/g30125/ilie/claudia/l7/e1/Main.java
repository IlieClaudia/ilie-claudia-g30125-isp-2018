package g30125.ilie.claudia.l7.e1;

public class Main {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Nume1",1000);
        BankAccount b2 = new BankAccount("Nume2",750);
        BankAccount b3 = new BankAccount("Nume2",2000);

        if(b1.equals(b2))
            System.out.println("Equals.");
        else System.out.println("Not equals.");

        if(b2.equals(b3))
            System.out.println("Equals.");
        else System.out.println("Not equals.");
    }
}
