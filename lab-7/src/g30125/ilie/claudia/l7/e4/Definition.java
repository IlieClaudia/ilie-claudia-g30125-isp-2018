package g30125.ilie.claudia.l7.e4;

public class Definition {
    String definition;

    public Definition(String definition)
    {
        this.definition = definition;
    }

    public String getDefinition(){
        return this.definition;
    }
}
