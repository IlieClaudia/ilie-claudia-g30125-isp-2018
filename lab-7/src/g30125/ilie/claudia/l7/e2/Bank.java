package g30125.ilie.claudia.l7.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank {

    ArrayList<BankAccount> b = new ArrayList<>();

    void addAccount(String owner, double balance) {
        BankAccount bankAccount = new BankAccount(owner, balance);
        b.add(bankAccount);
        // sau    b.add(new BankAccount(owner,balance);
    }

    class Comp implements Comparator<BankAccount> {
        @Override
        public int compare(BankAccount e1, BankAccount e2) {
            return Double.compare(e1.getBalance(), e2.getBalance());
        }
    }

    void printAccounts() {
        Collections.sort(b, new Comp());
        for (BankAccount str : b) {
            System.out.println(str.getOwner() + "  " + str.getBalance());
        }
    }

    void printAccounts(double minBalance, double maxBalance) {
        Collections.sort(b, new Comp());
        for (BankAccount bankAccount : b)
            if (minBalance < bankAccount.getBalance() && maxBalance > bankAccount.getBalance())
                System.out.println(bankAccount.getOwner() + " " + bankAccount.getBalance());
            else System.out.println("It s not between min and max");
    }

//    public BankAccount getAllAccount(String owner) {
//        for (BankAccount str : b) {
//            if (str.getOwner().equals(owner))
//                return str;
//        }
//        return null;
//    }

    public ArrayList<BankAccount> getAllAccount() {
        return b;
    }


    public static void main(String[] args) {

        Bank b = new Bank();

        b.addAccount("nume1", 1000);
        b.addAccount("ume2", 500);
        b.addAccount("nme3", 7500);
        System.out.println("Sorted list by balance:");
        b.printAccounts();

        System.out.println();
        System.out.println("Display all accounts between min range and max range values:");
        b.printAccounts(200, 3000);

        ArrayList<BankAccount> bankAccounts = b.getAllAccount();

        Collections.sort(bankAccounts,new BankAccount.ownerComparator());

        System.out.println();
        System.out.println("All bank accounts,sort alphabetically by owner field:");
        for(BankAccount b1:bankAccounts)
            System.out.println(b1.getBalance()+" "+b1.getOwner());
        }
    }
