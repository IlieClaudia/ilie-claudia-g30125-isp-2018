package g30125.ilie.claudia.l7.e2;

import java.util.Comparator;
import java.util.Objects;

public class BankAccount implements Comparable {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        super();
        this.owner = owner;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    void withdraw(double amount) {
        double bal = getBalance();
        if (bal < amount) {
            System.out.println("Too less money in account");

        } else {
            setBalance(balance - amount);
            System.out.println("The amount is " + getBalance());
        }
    }

    void deposit(double amount) {
        setBalance(balance + amount);
        System.out.println("The amount is " + getBalance());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return that.owner.equals(owner) &&
                Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }

    @Override
    public int compareTo(Object b) {
        return 0;
    }

    public static class ownerComparator implements Comparator<BankAccount>{
        public int compare(BankAccount b1, BankAccount b2) {
            return b1.getOwner().compareTo(b2.getOwner());
        }

    }
}
