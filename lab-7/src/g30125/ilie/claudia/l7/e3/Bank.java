package g30125.ilie.claudia.l7.e3;

import java.util.Comparator;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Bank {

    TreeSet<BankAccount> b = new TreeSet<>();

    void addAccount(String owner, double balance) {

          b.add(new BankAccount(owner,balance));
    }

    class Comp implements Comparator<BankAccount> {
        @Override
        public int compare(BankAccount e1, BankAccount e2) {
            return Double.compare(e1.getBalance(), e2.getBalance());
        }
    }

    void printAccounts() {
        Comparator<BankAccount> byBalance=Comparator.comparingDouble(BankAccount::getBalance);
        Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byBalance);

        TreeSet<BankAccount> sort = new TreeSet<>();
        sort=b.stream().collect(Collectors.toCollection(suplier));
        for(BankAccount b: sort)
            System.out.println(b.getBalance()+" "+b.getOwner());

    }

    void printAccounts(double minBalance, double maxBalance) {
        for(BankAccount bankAccount:b)
            if(bankAccount.getBalance()>minBalance && bankAccount.getBalance()<maxBalance)
                System.out.println(bankAccount.getBalance()+" "+bankAccount.getOwner());
    }

public TreeSet<BankAccount> getAllAcount(){
     Comparator<BankAccount> byName=Comparator.comparing(BankAccount::getOwner);
      Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byName);

      return b.stream().collect(Collectors.toCollection(suplier));

    }


    public static void main(String[] args) {

        Bank b = new Bank();

        b.addAccount("nume1", 1000);
        b.addAccount("ume2", 500);
        b.addAccount("nme3", 7500);
        System.out.println("Sorted list by balance:");
        b.printAccounts();

        System.out.println();
        System.out.println("Display all accounts between min range and max range values:");
        b.printAccounts(200, 3000);

        System.out.println();
        System.out.println("All bank accounts,sort alphabetically by owner field:");
        for(BankAccount b1:b.getAllAcount())
            System.out.println(b1.getOwner()+" "+b1.getBalance());
    }
}
