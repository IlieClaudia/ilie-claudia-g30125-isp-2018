package g30125.ilie.claudia.l8.e2;

import java.io.*;
import java.nio.file.*;

    public class CountLetter {
        private char lookFor;
        private Path file;

        CountLetter(char lookFor, Path file) {
            this.lookFor = lookFor;
            this.file = file;
        }

        public char getLookFor() {
            return lookFor;
        }

        public int count() throws IOException {
            int count = 0;
            try (InputStream in = Files.newInputStream(file);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(in)))
            {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    for (int i = 0; i < line.length(); i++) {
                        if (lookFor == line.charAt(i)) {
                            count++;
                        }
                    }
                }
            } catch (IOException x) {
                System.err.println(x);
            }
            return count;
        }

    }

