package g30125.ilie.claudia.l8.e2;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {

        Path file = Paths.get("data.txt");
        CountLetter c = new CountLetter('e',file);
        int i = c.count();
        System.out.format("File '%s' has %d instances of letter '%c'.%n", file, i, c.getLookFor());


    }
}
