package g30125.ilie.claudia.l8.e1;


public class CoffeTest {
    public static void main(String[] args) {
        CofeMaker mk = new CofeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for (int i = 0; i < 15; i++) {
            Cofee c = null;
            try {
                c = mk.makeCofee();
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception: " + e.getMessage() + " temp = " + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception: " + e.getMessage() + " conc = " + e.getConc());
            } catch (NumberException e) {
                System.out.println("Exception: " + e.getMessage() + " nr = " + e.getNumber());
            } finally {
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}
