package g30125.ilie.claudia.l8.e1;

public class CofeMaker {
    int nr = 0;
    int limit = 10;
    Cofee makeCofee() throws NumberException{
        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        this.nr++;
        if ( nr > limit )
            throw new NumberException(limit,"You can make only "+limit+" coffees!");
        Cofee cofee = new Cofee(t, c);
        return cofee;
    }
}
