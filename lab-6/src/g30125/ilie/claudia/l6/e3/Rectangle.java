package g30125.ilie.claudia.l6.e3;

import java.awt.*;

public class Rectangle implements Shape{

    private int length,width,height;
    private Color color;
    private int x,y;
    private String id;
    private boolean fill;

    public Rectangle(Color color, int x, int y, String id, boolean fill, int length, int width, int height) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return length;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public boolean getFill(){
        return fill;
    }



    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString()+"with id "+getId());
        g.setColor(getColor());
        if(getFill()==true)
            g.fillRect(getX(),getY(),getWidth(),getHeight());
        g.drawRect(getX(),getY(),getWidth(),getHeight());
    }
}
