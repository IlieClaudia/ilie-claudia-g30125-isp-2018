package g30125.ilie.claudia.l6.e3;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard  extends JFrame {

      Shape[] shapes = new Shape[10];

    //ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                //System.out.println("Adaugat obiect pe ["+i+"]");
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null) {
                shapes[i].draw(g);
            }
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }

    public void deleteByID(String id){
        for(int i=0; i<=shapes.length; i++)
            if(shapes[i]!=null) {
                shapes[i] = null;
                break;
            }
        this.repaint();
    }

    }



