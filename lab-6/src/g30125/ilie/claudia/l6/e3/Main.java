package g30125.ilie.claudia.l6.e3;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED,100,200, "i",false,90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 20,20,"ii",true,100);
        b1.addShape(s2);
        Shape r1 = new Rectangle(Color.BLUE,100,100,"iii",false,250 , 25,100);
        b1.addShape(r1);
        b1.deleteByID("iii");
    }
}
