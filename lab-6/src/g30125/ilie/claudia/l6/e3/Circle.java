package g30125.ilie.claudia.l6.e3;
import java.awt.*;

public class Circle implements Shape{

    private int radius;
    private Color color;
    private int x,y;
    private String id;
    private boolean fill;

    public Circle(Color color, int x, int y, String id, boolean fill,int radius) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
        this.radius = radius;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public boolean getFill(){
        return fill;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString()+"with id "+getId());
        g.setColor(getColor());
        if(getFill()==true)
            g.fillOval(getX(),getY(),getRadius(),getRadius());
        g.drawOval(getX(),getY(),getRadius(),getRadius());
    }
}


