package g30125.ilie.claudia.l6.e5;

import g30125.ilie.claudia.l6.e3.Shape;

import java.awt.*;
import java.util.Scanner;

public class Pyramid {


    private static int numberOfBricks;

    public static int getNumberOfBricks() {
        return numberOfBricks;
    }

    public static void setNumberOfBricks(int numberOfBricks1) {
        numberOfBricks = numberOfBricks1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int width, height;
        System.out.println("Number of bricks");
        setNumberOfBricks(in.nextInt());
        width = 20;
        height = 15;
//        System.out.println("Width = ");
//        width = in.nextInt();
//        System.out.println("Height = ");
//        height = in.nextInt();
        int initialX = 150;
        int drawingX = 0;
        DrawingBoard bl = new DrawingBoard();
        int bricksPerLevel = 1;
        while (getNumberOfBricks() >= bricksPerLevel) {
            if (bricksPerLevel % 2 == 1) drawingX =  initialX -  (bricksPerLevel/2)*width;
            else drawingX =  initialX - (bricksPerLevel/2)*width+width/2;
            for (int i = 0; i < bricksPerLevel; i++)
            {
                Rectangle x = new Rectangle(drawingX+i*width,100+bricksPerLevel*height,10,width,height);
                bl.addShape(x);

            }
            setNumberOfBricks(getNumberOfBricks()-bricksPerLevel);
            bricksPerLevel++;

        }
        System.out.println(getNumberOfBricks()+" "+bricksPerLevel);
    }
}
