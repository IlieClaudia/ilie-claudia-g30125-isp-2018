package g30125.ilie.claudia.l6.e5;

import g30125.ilie.claudia.l6.e3.Shape;

import java.awt.*;


public class Rectangle implements Shape{
    private int length;
    private int x,y,w,h;

    public Rectangle( int x, int y, int length, int w, int h) {
        this.x = x;
        this.y = y;
        this.length = length;
        this.w = w;
        this.h = h;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    public int getW() {
        return w;
    }
    public int getH() {
        return h;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void draw(Graphics g) {

        System.out.println("Block{" + "w=" + getW() + ", h=" + getH() + '}');
        g.drawRect(getX(),getY(),getW(),getH());
    }
}
