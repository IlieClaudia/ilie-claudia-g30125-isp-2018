package g30125.ilie.claudia.l6.e1;

import java.awt.*;

public abstract class Shape implements g30125.ilie.claudia.l6.e3.Shape {

    private Color color;
    private int x,y;
    private String id;
    private boolean fill;

    public Shape(Color color, int x, int y, String id, boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public boolean getFill(){
        return fill;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
