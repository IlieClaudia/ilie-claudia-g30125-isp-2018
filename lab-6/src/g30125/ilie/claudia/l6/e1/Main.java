package g30125.ilie.claudia.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Circle s1 = new Circle(Color.RED,200,200, "gs",true,90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 20,20,"is",false,100);
        b1.addShape(s2);
        Rectangle r1 = new Rectangle(Color.BLUE,100,100,"vvs",true,250);
        b1.addShape(r1);
        b1.deleteByID("gs");
    }
}
