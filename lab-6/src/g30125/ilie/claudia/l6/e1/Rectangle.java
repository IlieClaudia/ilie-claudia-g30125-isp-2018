package g30125.ilie.claudia.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int x, int y, String id, boolean fill, int length) {
        super(color,x,y,id,fill);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString()+"with id "+getId());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),100,100);
        if(getFill()==true)
            g.fillRect(getX(),getY(),100,100);
    }
}
