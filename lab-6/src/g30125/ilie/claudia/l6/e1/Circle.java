package g30125.ilie.claudia.l6.e1;

import java.awt.*;

    public class Circle extends Shape{

        private int radius;

        public Circle(Color color,int x, int y, String id, boolean fill,int radius ) {
            super(color,x,y,id,fill);
            this.radius = radius;
        }

        public int getRadius() {
            return radius;
        }

        @Override
        public void draw(Graphics g) {
            System.out.println("Drawing a circle "+this.radius+" "+getColor().toString()+"with id "+getId());
            g.setColor(getColor());
            g.drawOval(getX(),getY(),getRadius(),getRadius());
            if(getFill()==true)
                g.fillOval(getX(),getY(),getRadius(),getRadius());
        }
    }


