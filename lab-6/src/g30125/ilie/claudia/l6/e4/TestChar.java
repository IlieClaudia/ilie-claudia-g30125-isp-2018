package g30125.ilie.claudia.l6.e4;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestChar {

    @Test
    public void first() {
        CharSImplementation string = new CharSImplementation("asdfg");
        assertEquals(5, string.length());
    }

    @Test
    public void second() {
        CharSImplementation string = new CharSImplementation("asdfg");
        assertEquals('s',string.charAt(1));
    }

    @Test
    public void third() {
        CharSImplementation string = new CharSImplementation("asdfg");
        assertEquals("as", string.subSequence(0,2).toString());
    }
}
