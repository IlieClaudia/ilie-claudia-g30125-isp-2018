package g30125.ilie.claudia.l6.e4;

public class CharSImplementation implements CharSequence {

    private String s;

    public CharSImplementation( String s) {
        this.s = s;
    }

    @Override
    public int length() {
        return this.s.length();
    }

    @Override
    public char charAt(int index) {
        return s.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }
        if (end > s.length()) {
            throw new StringIndexOutOfBoundsException(end);
        }
        if (start > end) {
            throw new StringIndexOutOfBoundsException(start - end);
        }
        StringBuilder sub = new StringBuilder(s.subSequence(start, end));
        return sub;
    }


}
