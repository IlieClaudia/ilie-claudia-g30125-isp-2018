package g30125.ilie.claudia.l4.e4;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestAuthor {
    @Test
    public void testAuthor() {
        Author a1 = new Author("Aa", "Aa@", 'm');
        Author a2 = new Author("Ab", "Ab@", 'f');
        System.out.println(a1.toString());
        assertEquals("Aa",a1.getName());
    }
}
