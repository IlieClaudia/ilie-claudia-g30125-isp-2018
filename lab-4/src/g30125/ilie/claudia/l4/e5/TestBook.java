package g30125.ilie.claudia.l4.e5;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestBook {
    @Test
    public void testBook() {

        Book b1 = new Book("Bantuitii", "Aa", "Aa@", 'm', 200, 20);
        assertEquals("'Bantuitii' by Aa(m) at Aa@", b1.toString());
    }
}
