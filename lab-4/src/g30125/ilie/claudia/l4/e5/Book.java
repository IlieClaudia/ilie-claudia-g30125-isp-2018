package g30125.ilie.claudia.l4.e5;

import g30125.ilie.claudia.l4.e4.Author;

public class Book extends Author{
    private String name;
    private Author author;
    private double price;
    private int qtyInStock;

    public Book(){
        super();
        this.qtyInStock=0;
    }

    public Book(String name, String authorname, String email, char gender, double price){
        //this.author = new Author ("Cc","Cc@",'m');
        super(authorname,email,gender);
        this.name=name;
        this.price=price;
}

    public Book(String name, String authorname, String email, char gender, double price, int qtyInStock){
        super(authorname,email,gender);
        this.name=name;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public String getName(){
        return name;
    }

    public Author getAuthor(){
        return author;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price=price;
    }

    public int getQtyInStock(){
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock){
        this.qtyInStock=qtyInStock;
    }

    @Override

    public String toString(){
        return "'" + name + "'" + " by " + super.toString();
   }
}

