package g30125.ilie.claudia.l4.e7;;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCylinder {

    @Test
    public void testArea(){
        Cylinder c1 = new Cylinder(2.0,3.0);
        System.out.println("Aria gresita: " + c1.getAreaWrong());
        System.out.println("Aria corecta: " + c1.getArea());
        System.out.println("Volumul: " + c1.getVolume());
        System.out.println("-----------");
        assertEquals(31, c1.getArea(),001);
    }
    @Test
    public void testVolume(){
        Cylinder c2 = new Cylinder(4.0, 5.0);
        System.out.println("Aria gresita: " + c2.getAreaWrong());
        System.out.println("Aria corecta: " + c2.getArea());
        System.out.println("Volumul: " + c2.getVolume());
        assertEquals(251, c2.getVolume(), 001);
    }
}
