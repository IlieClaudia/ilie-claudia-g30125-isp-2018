package g30125.ilie.claudia.l4.e7;

public class Cylinder extends Circle{
    private double height;

    public Cylinder(){
        this.height=1.0;
    }

    public Cylinder(double radius){
        super(radius);
    }

    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getAreaWrong(){
        return Math.PI*getRadius()*getRadius();
    }

    public double getVolume(){
       // super(getRadius());
        return Math.PI*getRadius()*getRadius()*height;
    }

    public double getArea(){
        return Math.PI*getRadius()*(getRadius()+height);
    }
}
