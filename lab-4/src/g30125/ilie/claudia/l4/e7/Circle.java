package g30125.ilie.claudia.l4.e7;

public class Circle {
    private double radius;
    private String color;

    public Circle() {
        radius = 1.00;
        color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}





