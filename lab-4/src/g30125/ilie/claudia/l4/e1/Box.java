package g30125.ilie.claudia.l4.e1;

public class Box {
    private int id;

    public Box(Conveyor target, int pos, int id){
        this.id = id;
        target.addPackage(this,pos);
    }

    public int getId() {
        return id;
    }

    public String toString(){
        return ""+id;
    }
//Create a new function pickBox(int pos) which will remove a box from conveyor position 'pos';
//    public int pickBox(int pos){
//
//    }
}
