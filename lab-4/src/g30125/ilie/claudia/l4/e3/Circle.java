package g30125.ilie.claudia.l4.e3;

public class Circle {
    private double radius;
    private String color;

    public Circle(){
        radius=1;
        color="red";
    }


    public Circle(int radius){
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea(){
        return Math.PI*radius*radius;
    }

}
