package g30125.ilie.claudia.l4.e3;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestCircle {

    @Test
    public void testCircle() {
        Circle c1 = new Circle();
        System.out.println(c1.getRadius());
        System.out.println(c1.getArea());
        assertEquals(3.14,c1.getArea(),0.01);
    }
}
