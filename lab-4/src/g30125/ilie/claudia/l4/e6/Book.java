package g30125.ilie.claudia.l4.e6;

import g30125.ilie.claudia.l4.e4.Author;

public class Book extends Author{
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(){
        super();
        this.qtyInStock=0;
    }

    public Book(String name, Author[] authors, double price){
        //this.author = new Author ("Cc","Cc@",'m');
        this.name=name;
        this.authors=authors;
        this.price=price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock){
        this.name=name;
        this.authors=authors;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public String getName(){
        return name;
    }

    public Author[] getAuthor(){
        return authors;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public int getQtyInStock(){
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock){
        this.qtyInStock=qtyInStock;
    }

    public Author[] getAuthors() {
        return authors;
    }


    @Override

    public String toString(){
        return "'" + name + "'" + " by " +  authors.length+ " authors.";
    }

    public void printAuthors(){
       for(int i=0;i<authors.length;i++)
        {
            System.out.println(authors[i].getName());
        }
    }
}

