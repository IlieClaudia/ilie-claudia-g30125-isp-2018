package g30125.ilie.claudia.l4.e6;

import g30125.ilie.claudia.l4.e4.Author;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestBook {
    @Test
    public void testBook() {

        Author[] authors = new Author[3];
        Author a1 = new Author("Aa", "Aa@", 'm');
        Author a2 = new Author("Ab", "Ab@", 'f');
        Author a3 = new Author("Ac", "Ac@", 'f');
        Book b1 = new Book("Bb", authors, 200, 20);
        assertEquals("'Bb' by 3 authors.", b1.toString());
    }

    /*@Test
    public void testPrintAuthors(){
        Author[] authors = new Author[3];
        Author a1 = new Author("Aa", "Aa@", 'm');
        Author a2 = new Author("Ab", "Ab@", 'f');
        Author a3 = new Author("Ac", "Ac@", 'f');
        Book b1 = new Book("Bb", authors, 200, 20);
        assertEquals("WCRE",b1.printAuthors());}*/ //nu pot testa voiduri

    @Test
    public void testGetter() {
        Author[] authors = new Author[3];
        Author a1 = new Author("Aa", "Aa@", 'm');
        Author a2 = new Author("Ab", "Ab@", 'f');
        Author a3 = new Author("Ac", "Ac@", 'f');
        Book b1 = new Book("Bb", authors, 200, 20);
        assertEquals(200,b1.getPrice(),0.01);
    }
}

