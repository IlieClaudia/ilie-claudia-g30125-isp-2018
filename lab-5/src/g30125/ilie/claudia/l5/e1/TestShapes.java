package g30125.ilie.claudia.l5.e1;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestShapes {

    @Test
    public void testGetArea() {
        Shape[] s = new Shape[3];
        //Shape s1 = new Shape("green",0);
        s[1] = new Square(15);
        s[2] = new Rectangle(10, 20);
        s[0] = new Circle(2);
        assertEquals(200,s[2].getArea(),0.01);
        assertEquals(12.56,s[0].getArea(),0.01);
        assertEquals(225,s[1].getArea(),0.6);
    }

    @Test
    public void testGetPerimeter() {
        fail("Not yet implemented");
    }

}