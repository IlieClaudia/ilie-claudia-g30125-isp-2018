package g30125.ilie.claudia.l5.e3;

public class Controller {
    private static Controller control;
    TemperatureSensor tempSensor;
    LightSensor lightSensor;

    private Controller(){
    }

    public Controller (TemperatureSensor tempSensor, LightSensor lightSensor){
        this.tempSensor = tempSensor;
        this.lightSensor = lightSensor;
    }

    public static Controller getController(){
        if (control == null) {
            control = new Controller();
        }
        return control;
    }

    public void control() throws InterruptedException {
        Thread t = new Thread();
        System.out.println(tempSensor.readValue());
        t.sleep(1000);
        System.out.println(lightSensor.readValue());
    }
}

