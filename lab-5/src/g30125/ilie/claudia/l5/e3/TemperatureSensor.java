package g30125.ilie.claudia.l5.e3;

import java.util.Random;

public class TemperatureSensor extends Sensor {

    @Override
    public int readValue() {

        Random r = new Random();
        return r.nextInt(100);
    }
}
