package g30125.ilie.claudia.l5.e3;

import java.util.Scanner;

public abstract class Sensor {
    private String location;

    abstract int readValue();

    public String getLocation() {
        System.out.println("Enter the location:");
        Scanner in = new Scanner(System.in);
        location = in.nextLine();
        return location;
    }
}
