package g30125.ilie.claudia.l5.e3;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        TemperatureSensor tempSensor = new TemperatureSensor();
        LightSensor lightSensor = new LightSensor();

        tempSensor.getLocation();
        lightSensor.getLocation();

        System.out.println(tempSensor.getLocation());
        System.out.println(lightSensor.getLocation());

        System.out.println("\n");
        System.out.println("Test controller :");
        for (int i = 0; i < 20; i++) {
            Controller c = new Controller(tempSensor, lightSensor);
            c.control();
            Thread.sleep(1000);
        }

    }
}
