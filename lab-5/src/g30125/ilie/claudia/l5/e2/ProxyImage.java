package g30125.ilie.claudia.l5.e2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean b;

    public ProxyImage(String fileName, boolean b){
        this.fileName = fileName;
        this.b=b;
    }

    public ProxyImage(){}

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        if (b=true){
            realImage.display();
        }
        else {
            rotatedImage.display();
        }
    }
}