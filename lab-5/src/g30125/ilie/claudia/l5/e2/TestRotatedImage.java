package g30125.ilie.claudia.l5.e2;

import org.junit.Test;

public class TestRotatedImage {
    @Test
    public void testRotateImage(){
        Image r = new RotatedImage("90 degrees");
        r.display();
    }
    @Test
    public void testProxyImage(){
        Image r = new ProxyImage("jpg",true);
        r.display();
    }

    @Test
    public void testRealImage(){
        Image r = new RealImage("jpg");
        r.display();
        //r.loadFromDisk("--"); - este privata
    }
}
