package g30125.ilie.claudia.l2.e7;

import java.util.*;
//import java.util.Random;

public class ex7 {

	
	public static void main (String[] args) {
		Random n = new Random();
		Scanner in = new Scanner(System.in);
		int guess = n.nextInt(100);
	
		//System.out.println(guess);
		//intrarea utilizatorului
		int userGuess = 0;
		//int noOfGuesses = 0;
		int k = 0;

		while (k<3 && userGuess != guess)
		{
			System.out.print("Guess the number:");
			userGuess = in.nextInt();
			if (userGuess<guess) {System.out.println("Too low");k++;}
			else if (userGuess>guess) {System.out.println("Too high");k++;}
		}
		if (k<3) System.out.println("Your guess is correct! Good Job!");
		else System.out.println("You failed!");
		System.out.println("The number was: "+guess);
		in.close();
		
	}
}
