//Giving a vector of N elements, display the maximum element in the vector.

package g30125.ilie.claudia.l2.e4;

import java.util.*;

public class Ex4 {
	/*static int maxim (int n, int v[]) {
		{
		  int max=v[0]; 
		  for(int i=0;i<n;i++) 
		        if(max<v[i]) 
		            max=v[i];
		  return max; 
		 }
		
	}*/
	public static void main (String[] args) {
		int nr,i;
		int max = -999999;
		Scanner in = new Scanner(System.in);
		//Citim in vector direct
		System.out.print("Dati n:");
		nr = in.nextInt();
		in.close();
		
		int [] vect = new int [nr];
		//System.out.println("Elementul maxim este egal cu: ");
		for ( i=0; i<nr; i++)
		{
			//maxim(i);
			System.out.print("vect["+i+"]=");
			vect[i] = in.nextInt();
			if (max<vect[i]) max = vect[i];
			//System.out.println();
		}
		System.out.println("Maximul din vector este "+max);
	}

}
