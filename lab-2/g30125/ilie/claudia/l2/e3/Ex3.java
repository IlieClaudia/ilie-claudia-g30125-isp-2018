//Write a program which display prime numbers between A and B, where A and B are read from console. 
//Display also how many prime numbers have been found.
package g30125.ilie.claudia.l2.e3;

import java.util.*;

public class Ex3 {
	static boolean prim (int n) {
			int i,s=0;
	        for (i=1;i<=n;i++)
	            if (n%i==0)
	                s++;
	        if(s==2) return true;
	        return false;
	}
	
	public static void main(String[] args) {
		int a,b,i,nr=0;
        Scanner in = new Scanner(System.in);
        System.out.print("Dati numarul de unde porneste intervalul : "); 
        a = in.nextInt();
        System.out.print("Dati numarul unde se opreste intervalul : "); 
        b = in.nextInt();
        in.close();
        
        for (i=a; i<=b; i++)
            if(prim(i)==true){
           System.out.println(i);
           nr++;
            }
        System.out.println("Numarul de numere prime este:"+nr);
	}	
	
}
