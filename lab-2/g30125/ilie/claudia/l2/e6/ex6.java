package g30125.ilie.claudia.l2.e6;
import java.util.Scanner;
public class ex6 {
	
	static int factorialNerec(int n)
	{
		int i = 1, prod = 1;
		for (i = 1;i<=n;i++) prod*=i;
		return prod;
	}
	
	
	static int factorialRec(int n)
	{
		if (n == 0) return 1;
		else return n*factorialRec(n-1);
	}
	
	public static void main (String[] args) {
		int n;
		Scanner in = new Scanner(System.in);
		System.out.print("n=");
		n = in.nextInt();
		in.close();
		System.out.println("Factorial nerecursiv este: "+factorialNerec(n));
		System.out.println("Factorial recursiv este: "+factorialRec(n));
		
	}
	
	
}
