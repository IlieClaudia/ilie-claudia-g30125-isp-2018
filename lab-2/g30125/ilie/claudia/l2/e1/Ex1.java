
package g30125.ilie.claudia.l2.e1;

import java.util.Scanner;

public class Ex1 {
	
	public static void main(String[] args) {
		int a,b;
		Scanner in=new Scanner(System.in);
		System.out.println("Dati a:");
		a = in.nextInt();
		System.out.println("Dati b:");
		b = in.nextInt();
		in.close();
		if(a>b) 
			System.out.println("Maximul este a="+a);
		else if (a==b)
			System.out.println("Cele 2 numere sunt egale si au valoarea: "+a);
		else 
			System.out.println("Maximul este b="+b);
	}
}
