/*A class called MyPoint, which models a 2D point with x and y coordinates contains:

Two instance variables x (int) and y (int).
A �no-argument� (or �no-arg�) constructor that construct a point at (0, 0).
A constructor that constructs a point with the given x and y coordinates.
Getter and setter for the instance variables x and y.
A method setXY() to set both x and y.
A toString() method that returns a string description of the instance in the format �(x, y)�.

A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.

Write the code for the class MyPoint. Also write a test class (called TestMyPoint) to test all the methods defined in the class.*/


public class MyPoint {
	private int x,y;
	MyPoint()
	{
		x=0;
		y=0;
	}
	public MyPoint(int x,int y)
	{
		this.x = x;
		this.y = y;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	void setX(int x)
	{
		this.x=x;
	}
	void setY(int y)
	{
		this.y=y;
	}
	void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	String toString(int x, int y)
	{
		return ("("+ "this.x"+"this.y"+")");
	}
	
	public double distance (int x, int y) {
		return Math.sqrt((this.x - x) * (this.x - x) + (this.y - y)* (this.y - y));
	}

	
	public double distance(MyPoint secondPoint) {
		//return distance(this, secondPoint);
		return Math.sqrt((this.x - secondPoint.x) * (this.x - secondPoint.x) + (this.y - secondPoint.y)* (this.y - secondPoint.y));
	}



}
