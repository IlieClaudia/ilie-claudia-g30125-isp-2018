//Write a program that creates a robot at (1, 1) that moves north five times, turns around, and returns to its starting point.

import becker.robots.*;

public class exercise3 {
	public static void main (String[] args) {
		 // Set up the initial situation
		  City bucharest = new City();
	      //Thing parcel = new Thing(bucharest, 1, 2);
	      Robot ionel = new Robot(bucharest, 1, 1, Direction.NORTH);
	 
			// Direct the robot to the final situation

	      ionel.move();
	      ionel.move();
	      ionel.move();
	      ionel.move();
	      ionel.move();
	      ionel.turnLeft();
	      ionel.turnLeft();
	      ionel.move();
	      ionel.move();
	      ionel.move();
	      ionel.move();
	      ionel.move();
}
}
