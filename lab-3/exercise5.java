/*Every morning karel is awakened when the newspaper, represented by a Thing,
is thrown on the front porch of its house. Instruct karel to retrieve the newspaper
and return to �bed.� The initial situation is as shown in Figure 1-29; in the
final situation, karel is on its original intersection, facing its original direction,
with the newspaper.*/

import becker.robots.*;

public class exercise5 {
	public static void main (String[] args) {
		City oradea = new City();
		Thing parcel = new Thing(oradea, 2, 2);
		Wall o0 = new Wall(oradea, 1, 1, Direction.WEST);
		Wall o1 = new Wall(oradea, 2, 1, Direction.WEST);
		Wall o2 = new Wall(oradea, 1, 2, Direction.EAST);
		Wall o3 = new Wall(oradea, 1, 1, Direction.NORTH);
		Wall o4 = new Wall(oradea, 1, 2, Direction.NORTH);
		Wall o5 = new Wall(oradea, 2, 1, Direction.SOUTH);
		Wall o6 = new Wall(oradea, 1, 2, Direction.SOUTH);
		Robot cioara = new Robot(oradea, 1, 2, Direction.SOUTH);

		// Direct the robot to the final situation
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.move();
		cioara.turnLeft();
		cioara.move();
		cioara.turnLeft();
		cioara.move();
		cioara.pickThing();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.move();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.move();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.move();
		cioara.turnLeft();
		cioara.turnLeft();
		cioara.turnLeft();
	}	

}
