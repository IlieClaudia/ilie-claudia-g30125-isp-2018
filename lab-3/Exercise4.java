/*Write a program that begins with the initial situation shown in Figure 1-28.
Instruct the robot to go around the walls counter clockwise and return to its
starting position. Hint: Setting up the initial situation requires eight walls.*/

import becker.robots.*;

public class Exercise4 {
	public static void main (String[] args) {
		  City viena = new City();
	      Wall v0 = new Wall(viena, 1, 1, Direction.WEST);
	      Wall v1 = new Wall(viena, 2, 1, Direction.WEST);
	      Wall v2 = new Wall(viena, 1, 2, Direction.EAST);
	      Wall v3 = new Wall(viena, 2, 2, Direction.EAST);
	      Wall v4 = new Wall(viena, 1, 1, Direction.NORTH);
	      Wall v5 = new Wall(viena, 1, 2, Direction.NORTH);
	      Wall v6 = new Wall(viena, 2, 1, Direction.SOUTH);
	      Wall v7 = new Wall(viena, 2, 2, Direction.SOUTH);
	      Robot naruto = new Robot(viena, 0, 2, Direction.WEST);
	 
			// Direct the robot to the final situation
	      naruto.move();
	      naruto.move();
	      naruto.turnLeft();	// start turning right as three turn lefts
	      naruto.move();
	      naruto.move();
	      naruto.move();
	      naruto.turnLeft();
	      naruto.move();
	      naruto.move();
	      naruto.move();
	      naruto.turnLeft();
	      naruto.move();
	      naruto.move();
	      naruto.move();
	      naruto.turnLeft();
	      naruto.move();

	}

}
